import React from "react";
import { connect } from "react-redux";
import {
  setParamsAction,
  addNodeAction,
  delNodeAction,
  setNodeParamsAction,
  addCoinNodeAction,
  delCoinNodeAction,
  setCoinNodeParamsAction,
  setCdpParamsAction,
  setCdpRedeemParamsAction,
} from "@/store/index/action";
import PropTypes from "prop-types";
import JSONTree from "react-json-tree";
import Module from "./Module";
import { Input, Button } from "antd";
import "./index.scss";

const mapStateToProps = state => {
  return {
    contractInvokeParams: state.index.get("contractInvokeParams").toJS(),
    // nodeVoteParams: state.index.get("nodeVoteParams").toJS(),
    contractIssueParams: state.index.get("contractIssueParams").toJS(),
    transferParams: state.index.get("transferParams").toJS(),
    uContractInvokeParams: state.index.get("uContractInvokeParams").toJS(),
    assetIssueParams: state.index.get("assetIssueParams").toJS(),
    assetUpdateParams: state.index.get("assetUpdateParams").toJS(),
    uCoinTransferParams: state.index.get("uCoinTransferParams").toJS(),
    cdpCreateParams: state.index.get("cdpCreateParams").toJS(),
    cdpRedeemParams: state.index.get("cdpRedeemParams").toJS(),
    cdpLiquidParams: state.index.get("cdpLiquidParams").toJS(),
    dexCancelOrderParams: state.index.get("dexCancelOrderParams").toJS(),
    dexMarketParams: state.index.get("dexMarketParams").toJS(),
    dexLimitParams: state.index.get("dexLimitParams").toJS(),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setParams: payload => {
      dispatch(setParamsAction(payload));
    },
    addNode: () => {
      dispatch(addNodeAction());
    },
    delNode: payload => {
      dispatch(delNodeAction(payload));
    },
    setNodeParams: payload => {
      dispatch(setNodeParamsAction(payload));
    },
    addCoinNode: () => {
      dispatch(addCoinNodeAction());
    },
    delCoinNode: payload => {
      dispatch(delCoinNodeAction(payload));
    },
    setCoinNodeParams: payload => {
      dispatch(setCoinNodeParamsAction(payload));
    },
    setCdpParams: payload => {
      dispatch(setCdpParamsAction(payload));
    },
    setCdpRedeemParams: payload => {
      dispatch(setCdpRedeemParamsAction(payload));
    }
  };
};

@connect(
  mapStateToProps,
  mapDispatchToProps
)
class Index extends React.Component {
  constructor() {
    super();
    this.state = {
      nodeRes: null,
      uCoinTransferRes: null,
      contractIssueRes: null,
      transferRes: null,
      uContractInvokeRes: null,
      assetIssueRes: null,
      assetUpdateRes: null,
      getAddInfoRes: null,
      cdpCreateRes:null,
      cdpLiquidRes:null,
      cdpRedeemRes:null,
      dexLimitRes:null,
      dexMarketRes:null,
      dexCancelRes:null,

    };
  }
  handleChange(e, type) {
    this.props.setParams({
      name: e.target.name,
      value: e.target.value,
      type: type
    });
  }

  handleChangeNode(e, index) {
    this.props.setNodeParams({
      name: e.target.name,
      value: e.target.value,
      index: index
    });
  }

  handleCoinChangeNode(e, index) {
    this.props.setCoinNodeParams({
      name: e.target.name,
      value: e.target.value,
      index: index
    });
  }

  handelCdpParamsAction(e, index) {
    this.props.setCdpParams({
      name: e.target.name,
      value: e.target.value,
      index: index
    });
  }

  handelCdpRedeemParamsAction(e, index) {
    this.props.setCdpRedeemParams({
      name: e.target.name,
      value: e.target.value,
      index: index
    });
  }

  contractInvoke() {
    const _this = this;
    const { setParams, contractInvokeParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginContractInvoke",
      {
        ...contractInvokeParams,
        ...{
          inputAmount: String(
            Number(contractInvokeParams.inputAmount) * Math.pow(10, 8)
          )
        }
      },
      function (res) {
        console.log(res);
        setParams({ type: "contractInvokeParams", name: "res", value: res });
      },
      function (err) {
        console.log(err);
        _this.props.setParams({
          type: "contractInvokeParams",
          name: "res",
          value: err
        });
      }
    );
  }

  getAddInfo() {
    const _this = this;
    window.waykiBridge.walletPlugin(
      "getAddressInfo",
      {},
      function (res) {
        _this.setState({ getAddInfoRes: res });
      },
      function (err) {
        _this.setState({ getAddInfoRes: err });
      }
    );
  }

  // nodeVote() {
  //   const _this = this;
  //   const { nodeVoteParams } = this.props;
  //   window.waykiBridge.walletPlugin(
  //     "walletPluginNodeVote",
  //     {
  //       nodeList: nodeVoteParams.map(item => ({
  //         ...item,
  //         voteCount: Math.floor(
  //           String(Number(item.voteCount) * Math.pow(10, 8))
  //         )
  //       }))
  //     },
  //     function(res) {
  //       _this.setState({ nodeRes: res });
  //     },
  //     function(err) {
  //       _this.setState({ nodeRes: err });
  //     }
  //   );
  // }

  // 多币种转账
  uCoinTransfer() {
    const _this = this;
    const { uCoinTransferParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginUCoinTransfer",
      {
        destArr: uCoinTransferParams.destArr.map(item => ({
          ...item,
          amount: String(Math.floor(Number(item.amount) * Math.pow(10, 8)))
        })),
        memo: uCoinTransferParams.memo,
        genSign: uCoinTransferParams.genSign
      },
      function (res) {
        _this.setState({ uCoinTransferRes: res });
      },
      function (err) {
        _this.setState({ uCoinTransferRes: err });
      }
    );
  }

  contractIssue() {
    const _this = this;
    const { contractIssueParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginContractIssue",
      contractIssueParams,
      function (res) {
        _this.setState({ contractIssueRes: res });
      },
      function (err) {
        _this.setState({ contractIssueRes: err });
      }
    );
  }
  // 转账
  transfer() {
    const _this = this;
    const { transferParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginTransfer",
      {
        ...transferParams,
        amount: String(
          Math.floor(Number(transferParams.amount) * Math.pow(10, 8))
        )
      },
      function (res) {
        _this.setState({ transferRes: res });
      },
      function (err) {
        _this.setState({ transferRes: err });
      }
    );
  }

  // 多币种合约调用
  uContractInvoke() {
    const _this = this;
    const { uContractInvokeParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginUContractInvoke",
      {
        ...uContractInvokeParams,
        amount: String(Number(uContractInvokeParams.amount) * Math.pow(10, 8))
      },
      function (res) {
        _this.setState({ uContractInvokeRes: res });
      },
      function (err) {
        _this.setState({ uContractInvokeRes: err });
      }
    );
  }

  setGenSignChange (e) {
    this.props.setParams({ type: "uCoinTransferParams", name: "genSign", value: e.target.value });
  }

  // 资产发布
  assetIssue() {
    const _this = this;
    const { assetIssueParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginAssetIssue",
      {
        ...assetIssueParams,
        assetSupply: String(
          Number(assetIssueParams.assetSupply) * Math.pow(10, 8)
        )
      },
      function (res) {
        _this.setState({ assetIssueRes: res });
      },
      function (err) {
        _this.setState({ assetIssueRes: err });
      }
    );
  }

  // 资产更新
  assetUpdate() {
    const _this = this;
    const { assetUpdateParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginAssetUpdate",
      {
        ...assetUpdateParams,
        updateValue:
          assetUpdateParams.updateType == 3
            ? String(Number(assetUpdateParams.updateValue) * Math.pow(10, 8))
            : assetUpdateParams.updateValue
      },
      function (res) {
        _this.setState({ assetUpdateRes: res });
      },
      function (err) {
        _this.setState({ assetUpdateRes: err });
      }
    );
  }

   // 创建CDP
  cdpCreate() {
    const _this = this;
    const { cdpCreateParams } = this.props;
    window.waykiBridge.walletPlugin(
      "walletPluginCdpStake",
      {
        destArr: cdpCreateParams.assetMap.map(item => ({
          ...item,
          bCoinToStake: String(Math.floor(Number(item.bCoinToStake) * Math.pow(10, 8)))
        })),
        scoinSymbol: cdpCreateParams.scoinSymbol,
        scoinNum: String(Math.floor(Number(cdpCreateParams.scoinNum) * Math.pow(10, 8))),
        cdpTxID: cdpCreateParams.cdpTxID,
      },
      function (res) {
        _this.setState({ cdpCreateRes: res });
      },
      function (err) {
        _this.setState({ cdpCreateRes: err });
      }
    );
  }

     // CDP赎回
     cdpRedeem() {
      const _this = this;
      const { cdpRedeemParams } = this.props;
      window.waykiBridge.walletPlugin(
        "walletPluginCdpRedeem",
        {
          destArr: cdpRedeemParams.assetMap.map(item => ({
            ...item,
            redeemCoinNum: String(Math.floor(Number(item.redeemCoinNum) * Math.pow(10, 8)))
          })),
          repayNum: String(Math.floor(Number(cdpRedeemParams.repayNum) * Math.pow(10, 8))),
          cdpTxID: cdpRedeemParams.cdpTxID,
        },
        function (res) {
          _this.setState({ cdpRedeemRes: res });
        },
        function (err) {
          _this.setState({ cdpRedeemRes: err });
        }
      );
    }

    cdpLiquid(){
      const _this = this;
      const { cdpLiquidParams } = this.props;
      window.waykiBridge.walletPlugin(
        "walletPluginCdpLiquidate",
        {
          ...cdpLiquidParams,
        },
        function (res) {
          _this.setState({ cdpLiquidRes: res });
        },
        function (err) {
          _this.setState({ cdpLiquidRes: err });
        }
      );
    }

    dexLimit(){
      const _this = this;
      const { dexLimitParams } = this.props;
      window.waykiBridge.walletPlugin(
        "walletPluginDexLimit",
        {
          ...dexLimitParams,
        },
        function (res) {
          _this.setState({ dexLimitRes: res });
        },
        function (err) {
          _this.setState({ dexLimitRes: err });
        }
      );
    }

    dexMarket(){
      const _this = this;
      const { dexMarketParams } = this.props;
      window.waykiBridge.walletPlugin(
        "walletPluginDexMarket",
        {
          ...dexMarketParams,
        },
        function (res) {
          _this.setState({ dexMarketRes: res });
        },
        function (err) {
          _this.setState({ dexMarketRes: err });
        }
      );
    }

    dexCancel(){
      const _this = this;
      const { dexCancelOrderParams } = this.props;
      window.waykiBridge.walletPlugin(
        "walletPluginDexCancelOrder",
        {
          ...dexCancelOrderParams,
        },
        function (res) {
          _this.setState({ dexCancelRes: res });
        },
        function (err) {
          _this.setState({ dexCancelRes: err });
        }
      );
    }
  render() {
    const {
      transferParams,
      contractIssueParams,
      contractInvokeParams,
      // nodeVoteParams,
      uContractInvokeParams,
      assetIssueParams,
      assetUpdateParams,
      uCoinTransferParams,
      cdpCreateParams,
      cdpLiquidParams,
      cdpRedeemParams,
      dexLimitParams,
      dexMarketParams,
      dexCancelOrderParams,
      // addNode,
      // delNode,
      addCoinNode,
      delCoinNode
    } = this.props;
    const contractInvokeData = [
      {
        label: "regId",
        name: "regId",
        value: contractInvokeParams.regId
      },
      {
        label: "contractField",
        name: "contractField",
        value: contractInvokeParams.contractField
      },
      {
        label: "inputAmount",
        name: "inputAmount",
        value: contractInvokeParams.inputAmount,
        suffix: "wicc",
        transform:
          contractInvokeParams.inputAmount &&
          `${parseInt(
            Number(contractInvokeParams.inputAmount) * Math.pow(10, 8)
          )} sawi`
      },
      {
        label: "remark",
        name: "remark",
        value: contractInvokeParams.remark
      }
    ];
    const contractIssueData = [
      {
        label: "contractContent",
        name: "contractContent",
        value: contractIssueParams.contractContent
      },
      {
        label: "contractDesc",
        name: "contractDesc",
        value: contractIssueParams.contractDesc
      }
    ];
    const transferData = [
      {
        label: "amount",
        name: "amount",
        value: transferParams.amount,
        transform:
          transferParams.amount &&
          `${parseInt(Number(transferParams.amount) * Math.pow(10, 8))} sawi`
      },
      {
        label: "collectionAddress",
        name: "collectionAddress",
        value: transferParams.collectionAddress
      },
      {
        label: "remark",
        name: "remark",
        value: transferParams.remark
      }
    ];
    const uContractInvokeData = [
      {
        label: "amount",
        name: "amount",
        value: uContractInvokeParams.amount,
        transform:
          uContractInvokeParams.amount &&
          `${parseInt(
            Number(uContractInvokeParams.amount) * Math.pow(10, 8)
          )} sawi`
      },
      {
        label: "coinSymbol",
        name: "coinSymbol",
        value: uContractInvokeParams.coinSymbol
      },
      {
        label: "regId",
        name: "regId",
        value: uContractInvokeParams.regId
      },
      {
        label: "contract",
        name: "contract",
        value: uContractInvokeParams.contract
      },
      {
        label: "memo",
        name: "memo",
        value: uContractInvokeParams.memo
      },
      {
        label: "genSign",
        name: "genSign",
        value: uContractInvokeParams.genSign
      }
    ];

    const assetIssueData = [
      {
        label: "assetSymbol",
        name: "assetSymbol",
        value: assetIssueParams.assetSymbol
      },
      {
        label: "assetName",
        name: "assetName",
        value: assetIssueParams.assetName
      },
      {
        label: "assetSupply",
        name: "assetSupply",
        value: assetIssueParams.assetSupply,
        transform:
          assetIssueParams.assetSupply &&
          `${parseInt(
            Number(assetIssueParams.assetSupply) * Math.pow(10, 8)
          )} sawi`
      },
      {
        label: "assetOwnerId",
        name: "assetOwnerId",
        value: assetIssueParams.assetOwnerId
      },
      {
        label: "assetMintable",
        name: "assetMintable",
        value: assetIssueParams.assetMintable,
        transform: "是否可增发(true or false)"
      }
    ];
    const assetUpdateData = [
      {
        label: "assetSymbol",
        name: "assetSymbol",
        value: assetUpdateParams.assetSymbol
      },
      {
        label: "updateType(1,2,3)",
        name: "updateType",
        value: assetUpdateParams.updateType,
        placeholder: '1: ower addr, 2: name, 3: 增发量'
      },
      {
        label: "updateValue",
        name: "updateValue",
        value: assetUpdateParams.updateValue,
        transform:
          assetUpdateParams.updateValue && !window.isNaN(assetUpdateParams.updateValue) &&
          `${parseInt(
            Number(assetUpdateParams.updateValue) * Math.pow(10, 8)
          )} sawi`
      }
    ];
    const cdpLiquidData = [
      {
        label: "assetSymbol",
        name: "assetSymbol",
        value: cdpLiquidParams.assetSymbol
      },
      {
        label: "liquidateNum",
        name: "liquidateNum",
        value: cdpLiquidParams.liquidateNum,
        transform:
        cdpLiquidParams.liquidateNum && !window.isNaN(cdpLiquidParams.liquidateNum) &&
          `${parseInt(
            Number(cdpLiquidParams.liquidateNum) * Math.pow(10, 8)
          )} sawi`
      },
      {
        label: "cdpTxID",
        name: "cdpTxID",
        value: cdpLiquidParams.cdpTxID
      },
    ];
    const dexLimitData = [
      {
        label: "dexTxType",
        name: "dexTxType",
        value: dexLimitParams.dexTxType
      },
      {
        label: "coinType",
        name: "coinType",
        value: dexLimitParams.coinType
      },
      {
        label: "assetType",
        name: "assetType",
        value: dexLimitParams.assetType
      },
      {
        label: "amount",
        name: "amount",
        value: dexLimitParams.amount,
        transform:
        dexLimitParams.amount && !window.isNaN(dexLimitParams.amount) &&
          `${parseInt(
            Number(dexLimitParams.amount) * Math.pow(10, 8)
          )} sawi`
      },
      {
        label: "price",
        name: "price",
        value: dexLimitParams.price,
        transform:
        dexLimitParams.price && !window.isNaN(dexLimitParams.price) &&
          `${parseInt(
            Number(dexLimitParams.price) * Math.pow(10, 8)
          )} sawi`
      },
    ];

    const dexMarketData = [
      {
        label: "dexTxType",
        name: "dexTxType",
        value: dexMarketParams.dexTxType
      },
      {
        label: "coinType",
        name: "coinType",
        value: dexMarketParams.coinType
      },
      {
        label: "assetType",
        name: "assetType",
        value: dexMarketParams.assetType
      },
      {
        label: "amount",
        name: "amount",
        value: dexMarketParams.amount,
        transform:
        dexMarketParams.amount && !window.isNaN(dexMarketParams.amount) &&
          `${parseInt(
            Number(dexMarketParams.amount) * Math.pow(10, 8)
          )} sawi`
      },
    ];

    const dexCancelData = [
      {
        label: "dexTxNum",
        name: "dexTxNum",
        value: dexCancelOrderParams.dexTxNum
      },
      
    ];
    return (
      <div>
        <Module
          data={[]}
          title="getAddressInfo (Get Wallet Address Information)"
          handleSubmit={this.getAddInfo.bind(this)}
          res={this.state.getAddInfoRes}
        ></Module>
        {/* <Module
          data={contractInvokeData}
          title="walletPluginContractInvoke (旧合约调用接口，不建议使用)"
          handleChange={e => this.handleChange(e, "contractInvokeParams")}
          handleSubmit={this.contractInvoke.bind(this)}
          res={contractInvokeParams.res}
        ></Module> */}
        <Module
          data={contractIssueData}
          title="walletPluginContractIssue (Publish Contract)"
          handleChange={e => this.handleChange(e, "contractIssueParams")}
          handleSubmit={this.contractIssue.bind(this)}
          res={this.state.contractIssueRes}
        ></Module>
        <section className="module">
          <div className="title">walletPluginUCoinTransfer (多币种转账)</div>
          <div className="params">
            {uCoinTransferParams.destArr.map((item, index) => (
              <div className="node-item" key={index}>
                <div className="param">
                  <label>amount</label>
                  <Input
                    type="text"
                    name="amount"
                    onChange={e => this.handleCoinChangeNode(e, index)}
                    value={item.amount}
                  />{" "}
                  {uCoinTransferParams.destArr.length > 1 && (
                    <span className="del"></span>
                  )}
                </div>
                {item.amount && (
                  <div className="param transform">
                    <label></label>
                    <div>{item.amount * Math.pow(10, 8)} sawi</div>
                  </div>
                )}
                <div className="param">
                  <label>coinSymbol</label>
                  <Input
                    type="text"
                    name="coinSymbol"
                    onChange={e => this.handleCoinChangeNode(e, index)}
                    value={item.coinSymbol}
                  />{" "}
                  {uCoinTransferParams.destArr.length > 1 && (
                    <span className="del"></span>
                  )}
                </div>
                <div className="param">
                  <label>destAddr</label>
                  <Input
                    type="text"
                    name="destAddr"
                    onChange={e => this.handleCoinChangeNode(e, index)}
                    value={item.destAddr}
                  />{" "}
                  {uCoinTransferParams.destArr.length > 1 && (
                    <span
                      className="del"
                      onClick={delCoinNode.bind(this, index)}
                    >
                      删除
                    </span>
                  )}
                </div>
              </div>
            ))}
          </div>
          <div className="param">
            <label>genSign</label>
            <Input
              type="text"
              name="genSign"
              onChange={ e => this.setGenSignChange(e)}
              value={uCoinTransferParams.genSign}
            />
          </div>
          {this.state.uCoinTransferRes && (
            <JSONTree
              data={this.state.uCoinTransferRes}
              shouldExpandNode={() => true}
            />
          )}
          <div className="btn-group">
            {/* <Button className="first" block onClick={addCoinNode.bind(this)}>
              Add
            </Button> */}
            <Button
              type="primary"
              block
              onClick={this.uCoinTransfer.bind(this)}
            >
              Submit
            </Button>
          </div>
        </section>
        {/* <Module
          data={transferData}
          title="walletPluginTransfer (Transfer)"
          handleChange={e => this.handleChange(e, "transferParams")}
          handleSubmit={this.transfer.bind(this)}
          res={this.state.transferRes}
        ></Module> */}
        <Module
          data={uContractInvokeData}
          title="uContractInvokeData (多币种合约调用)"
          handleChange={e => this.handleChange(e, "uContractInvokeParams")}
          handleSubmit={this.uContractInvoke.bind(this)}
          res={this.state.uContractInvokeRes}
        ></Module>

        <Module
          data={assetIssueData}
          title="assetIssue (资产发布)"
          handleChange={e => this.handleChange(e, "assetIssueParams")}
          handleSubmit={this.assetIssue.bind(this)}
          res={this.state.assetIssueRes}
        ></Module>

        <Module
          data={assetUpdateData}
          title="assetUpdate (资产更新)"
          handleChange={e => this.handleChange(e, "assetUpdateParams")}
          handleSubmit={this.assetUpdate.bind(this)}
          res={this.state.assetUpdateRes}
        ></Module>

        
        <section className="module">
          <div>以下接口仅供手机端调用</div>
        </section>


        <section className="module">
          <div className="title">walletPluginCdpStake (cdp创建/追加)</div>
          <div className="params">
            {cdpCreateParams.assetMap.map((item, index) => (
              <div className="node-item" key={index}>
                <div className="param">
                  <label>bCoinToStake</label>
                  <Input
                    type="text"
                    name="bCoinToStake"
                    onChange={e => this.handelCdpParamsAction(e, index)}
                    value={item.bCoinToStake}
                  />{" "}
                  {cdpCreateParams.assetMap.length > 1 && (
                    <span className="del"></span>
                  )}
                </div>
                {item.bCoinToStake && (
                  <div className="param transform">
                    <label></label>
                    <div>{item.bCoinToStake * Math.pow(10, 8)} sawi</div>
                  </div>
                )}
                <div className="param">
                  <label>coinSymbol</label>
                  <Input
                    type="text"
                    name="coinSymbol"
                    onChange={e => this.handelCdpParamsAction(e, index)}
                    value={item.coinSymbol}
                  />{" "}
                  {cdpCreateParams.assetMap.length > 1 && (
                    <span className="del"></span>
                  )}
                </div>
              </div>
            ))}
          </div>
          <div className="param">
            <label>scoinSymbol</label>
            <Input
              type="text"
              name="scoinSymbol"
              onChange={e => this.handleChange(e, "cdpCreateParams")}
              value={cdpCreateParams.scoinSymbol}
            />
          </div>
          <div className="param">
            <label>scoinNum</label>
            <Input
              type="text"
              name="scoinNum"
              onChange={e => this.handleChange(e, "cdpCreateParams")}
              value={cdpCreateParams.scoinNum}
            />
          </div>
          {cdpCreateParams.scoinNum && (
                  <div className="param transform">
                    <label></label>
                    <div>{cdpCreateParams.scoinNum * Math.pow(10, 8)} sawi</div>
                  </div>
                )}
          <div className="param">
            <label>cdpTxID(不填为创建，填为追加)</label>
            <Input
              type="text"
              name="cdpTxID"
              onChange={e => this.handleChange(e, "cdpCreateParams")}
              value={cdpCreateParams.cdpTxID}
            />
          </div>
          
          
          <div className="btn-group">
            {/* <Button className="first" block onClick={addCoinNode.bind(this)}>
              Add
            </Button> */}
            <Button
              type="primary"
              block
              onClick={this.cdpCreate.bind(this)}
            >
              Submit
            </Button>
          </div>
          {this.state.cdpCreateRes && (
            <JSONTree
              data={this.state.cdpCreateRes}
              shouldExpandNode={() => true}
            />
          )}
        </section>

        <section className="module">
          <div className="title">walletPluginCdpRedeem（CDP赎回）</div>
          <div className="params">
            {cdpRedeemParams.assetMap.map((item, index) => (
              <div className="node-item" key={index}>
                <div className="param">
                  <label>redeemCoinNum</label>
                  <Input
                    type="text"
                    name="redeemCoinNum"
                    onChange={e => this.handelCdpRedeemParamsAction(e, index)}
                    value={item.redeemCoinNum}
                  />{" "}
                  {cdpRedeemParams.assetMap.length > 1 && (
                    <span className="del"></span>
                  )}
                </div>
                {item.redeemCoinNum && (
                  <div className="param transform">
                    <label></label>
                    <div>{item.redeemCoinNum * Math.pow(10, 8)} sawi</div>
                  </div>
                )}
                <div className="param">
                  <label>coinSymbol</label>
                  <Input
                    type="text"
                    name="coinSymbol"
                    onChange={e => this.handelCdpRedeemParamsAction(e, index)}
                    value={item.coinSymbol}
                  />{" "}
                  {cdpRedeemParams.assetMap.length > 1 && (
                    <span className="del"></span>
                  )}
                </div>
              </div>
            ))}
          </div>
          <div className="param">
            <label>repayNum</label>
            <Input
              type="text"
              name="repayNum"
              onChange={e => this.handleChange(e, "cdpRedeemParams")}
              value={cdpRedeemParams.repayNum}
            />
          </div>
          {cdpRedeemParams.repayNum && (
                  <div className="param transform">
                    <label></label>
                    <div>{cdpRedeemParams.repayNum * Math.pow(10, 8)} sawi</div>
                  </div>
                )}
          <div className="param">
            <label>cdpTxID</label>
            <Input
              type="text"
              name="cdpTxID"
              onChange={e => this.handleChange(e, "cdpRedeemParams")}
              value={cdpRedeemParams.cdpTxID}
            />
          </div>
          
          
          <div className="btn-group">
            {/* <Button className="first" block onClick={addCoinNode.bind(this)}>
              Add
            </Button> */}
            <Button
              type="primary"
              block
              onClick={this.cdpRedeem.bind(this)}
            >
              Submit
            </Button>
          </div>

          {this.state.cdpRedeemRes && (
            <JSONTree
              data={this.state.cdpRedeemRes}
              shouldExpandNode={() => true}
            />
          )}
        </section>
        

        <Module
          data={cdpLiquidData}
          title="walletPluginCdpLiquidate (CDP清算)"
          handleChange={e => this.handleChange(e, "cdpLiquidParams")}
          handleSubmit={this.cdpLiquid.bind(this)}
          res={this.state.cdpLiquidRes}
        ></Module>

        <Module
          data={dexLimitData}
          title="walletPluginDexLimit (Dex限价)"
          handleChange={e => this.handleChange(e, "dexLimitParams")}
          handleSubmit={this.dexLimit.bind(this)}
          res={this.state.dexLimitRes}
        ></Module>

        <Module
          data={dexMarketData}
          title="walletPluginDexMarket (Dex市价)"
          handleChange={e => this.handleChange(e, "dexMarketParams")}
          handleSubmit={this.dexMarket.bind(this)}
          res={this.state.dexMarketRes}
        ></Module>

        <Module
          data={dexCancelData}
          title="walletPluginDexCancelOrder (Dex取消)"
          handleChange={e => this.handleChange(e, "dexCancelOrderParams")}
          handleSubmit={this.dexCancel.bind(this)}
          res={this.state.dexCancelRes}
        ></Module>
        {/*<section className="module">
          <div className="title">walletPluginNodeVote (Vote)</div>
          <div className="params">
            {nodeVoteParams.map((item, index) => (
              <div className="node-item" key={index}>
                <div className="param">
                  <label>votedAddress</label>
                  <Input
                    type="text"
                    name="votedAddress"
                    onChange={e => this.handleChangeNode(e, index)}
                    value={item.votedAddress}
                  />{" "}
                  {nodeVoteParams.length > 1 && <span className="del"></span>}
                </div>
                <div className="param">
                  <label>voteCount</label>
                  <Input
                    type="text"
                    name="voteCount"
                    onChange={e => this.handleChangeNode(e, index)}
                    value={item.voteCount}
                  />{" "}
                  {nodeVoteParams.length > 1 && (
                    <span className="del" onClick={delNode.bind(this, index)}>
                      删除
                    </span>
                  )}
                </div>
              </div>
            ))}
          </div>
          {this.state.nodeRes && (
            <JSONTree data={this.state.nodeRes} shouldExpandNode={() => true} />
          )}
          <div className="btn-group">
            <Button className="first" block onClick={addNode.bind(this)}>
              Add
            </Button>
            <Button type="primary" block onClick={this.nodeVote.bind(this)}>
              Submit
            </Button>
          </div>
        </section> */}
      </div>
    );
  }
}

Index.defaultProps = {
  contractInvokeParams: {}
};

Index.propTypes = {
  contractInvokeParams: PropTypes.object.isRequired,
  setContractInvokeParams: PropTypes.func
};

export default Index;
